import React, { useState } from "react";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import MenuIcon from "@mui/icons-material/Menu";
import "../css/Navbar.css";
const Navbar = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  const toggleMobileMenu = () => {
    setShowMobileMenu(!showMobileMenu);
  };
  const navbgcolor = process.env.REACT_APP_NAVBAR_BACKGROUND_COLOR;
  const navbutbgcolor = process.env.REACT_APP_NAVBAR_BUTTON_BACKGROUND_COLOR;
  const navbutcolor = process.env.REACT_APP_NAVBAR_BUTTON_COLOR;
  const navmobgcolor =
    process.env.REACT_APP_NAVBAR_MOBILE_MENU_BACKGROUND_COLOR;
  return (
    <div className="navbar" style={{ "--navbar-background-color": navbgcolor }}>
      <div className="navbar-left">
        <CameraAltIcon />
        <span className="navbar-title">Blog App</span>
        <span className="navbar-title">About</span>
        <span className="navbar-title">Memory</span>
      </div>
      <div className="navbar-right">
        <button
          className="navbar-button"
          style={{
            "--navbar-button-background-color": navbutbgcolor,
            "--navbar-button-color": navbutcolor,
          }}
        >
          Login
        </button>
      </div>
      <div className="navbar-mobile">
        <div className="navbar-mobile-left">
          <CameraAltIcon />
        </div>

        <MenuIcon onClick={toggleMobileMenu} />

        {showMobileMenu && (
          <div
            className="navbar-mobile-menu"
            style={{ "--navbar-mobile-menu-background-color": navmobgcolor }}
          >
            <button className="navbar-button">Login</button>
            <span className="navbar-title">Blog App</span>
            <span className="navbar-title">About</span>
            <span className="navbar-title">Memory</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
