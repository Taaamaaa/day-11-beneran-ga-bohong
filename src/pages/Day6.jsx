import axios from "axios";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useToggle } from "@uidotdev/usehooks";
import Navbar from "../assets/components/Navbar";

function LayoutPage(props) {
  const {
    postData: initialPostData,
    isMobile: initialIsMobile,
    darkMode: initialDarkMode,
  } = props;
  const [postData, setPostData] = useState(initialPostData);
  const [isMobile, setIsMobile] = useState(initialIsMobile);
  const [darkMode, toggleDarkMode] = useToggle(initialDarkMode);

  useEffect(() => {
    const fetchPostData = async () => {
      try {
        const response = await axios.get("http://localhost:3004/postgenerated");
        setPostData(response.data);
      } catch (error) {
        console.error("Error fetching post data:", error);
      }
    };

    fetchPostData();

    const handleResize = () => {
      setIsMobile(window.innerWidth <= 600);
    };

    handleResize();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      <Navbar />
      <div
        style={{
          maxWidth: "1200px",
          margin: "0 auto",
          backgroundColor: darkMode ? "#333333" : "#f9f9f9",
          padding: "20px",
          boxSizing: "border-box",
          fontFamily: "Arial, sans-serif",
        }}
      >
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
          }}
        >
          <div
            className="content"
            style={{
              flex: "2",
              backgroundColor: darkMode ? "#222222" : "#ffffff",
              padding: "20px",
              boxSizing: "border-box",
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {postData &&
              postData.map((post, index) => (
                <div
                  key={post.post_id}
                  className="card"
                  style={{
                    flex: isMobile ? "0 0 100%" : "0 0 calc(50% - 10px)",
                    backgroundColor: darkMode ? "#444444" : "#f7f7f7",
                    border: "1px solid #e5e5e5",
                    borderRadius: "5px",
                    marginBottom: "20px",
                    padding: "20px",
                    boxSizing: "border-box",
                    width: "100%",
                    maxWidth: isMobile ? "100%" : "calc(50% - 10px)",
                    marginRight: isMobile || index % 2 === 1 ? "0" : "20px",
                  }}
                >
                  <h3
                    style={{
                      marginBottom: "10px",
                      fontSize: "20px",
                      fontWeight: "bold",
                      color: darkMode ? "#ffffff" : "#333333",
                    }}
                  >
                    {post.title}
                  </h3>
                  <img
                    src={post.img}
                    alt="Post Image"
                    style={{
                      width: "100%",
                      height: "auto",
                      marginBottom: "10px",
                    }}
                  />
                  <p
                    style={{
                      fontSize: "16px",
                      fontWeight: "normal",
                      marginBottom: "5px",
                      color: darkMode ? "#ffffff" : "#555555",
                    }}
                  >
                    Author: {post.author} {post.lastName}
                  </p>
                  <p
                    style={{
                      fontSize: "16px",
                      fontWeight: "normal",
                      marginBottom: "5px",
                      color: darkMode ? "#ffffff" : "#555555",
                    }}
                  >
                    Date Posted: {post.datePost}
                  </p>
                  <p
                    style={{
                      fontSize: "16px",
                      fontWeight: "normal",
                      marginBottom: "0",
                      color: darkMode ? "#ffffff" : "#555555",
                    }}
                  >
                    {post.description}
                  </p>
                </div>
              ))}
          </div>
          {!isMobile && (
            <div
              className="sidebar"
              style={{
                flex: "1",
                backgroundColor: darkMode
                  ? process.env.REACT_APP_SIDEBAR_DARK
                  : process.env.REACT_APP_SIDEBAR,
                color: "#ffffff",
                padding: "20px",
                boxSizing: "border-box",
              }}
            >
              <h2>Sidebar</h2>
              <p>This is the sidebar content.</p>
            </div>
          )}
        </div>
      </div>
      <button
        onClick={toggleDarkMode}
        style={{
          position: "fixed",
          bottom: "20px",
          right: "20px",
          borderRadius: "50%",
          width: "50px",
          height: "50px",
          backgroundColor: darkMode ? "#ffffff" : "#333333",
          color: darkMode ? "#333333" : "#ffffff",
          border: "none",
          outline: "none",
          cursor: "pointer",
          transition: "background-color 0.3s, color 0.3s",
        }}
      >
        {darkMode ? "Light" : "Dark"}
      </button>
    </div>
  );
}

LayoutPage.propTypes = {
  postData: PropTypes.arrayOf(
    PropTypes.shape({
      post_id: PropTypes.string.isRequired,
      img: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      author: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      datePost: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
  isMobile: PropTypes.bool.isRequired,
  darkMode: PropTypes.bool.isRequired,
};

export default LayoutPage;
